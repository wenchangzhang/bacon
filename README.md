# bacon

This started out as a server to play Six Degrees of Kevin Bacon, but now serves up movie and actor information as well.

## Datasets
Datasets can be retrieved from <https://datasets.imdbws.com/>

Required files from above:
* title.basics.tsv.gz
* title.principals.tsv.gz
* name.basics.tsv.gz

## Starting up the REST server
1. Download this repo
1. `cd bacon`
1. Download the required datasets from above.
1. `virtualenv bacon`
1. `source bacon/bin/activate`
1. `pip install -r requirements.txt`
1. `python bacon.py`

The server will now be listening on port 9999.

## REST API

### Searching
`/search/title/q=<search string>`

Additional boolean parameters: `show_roles`, `show_cast`

Searches for the given title.  If an exact match is not found, the server attempts to search for a title starting with the given string.  It makes an attempt to substitute `&` for `and`, and vice versa.
A list of results is returned.


`/search/name/q=<search string>`

Additional boolean parameters: `show_roles`, `show_titles`, `show_details`

Searches for the given name, returning only a list of exact matches.


### Title Information
`/title/<titleID>`

Additional boolean parameters: `show_roles`, `show_cast`, `show_non_principals`

Provides more detailed information on the title.


### Name Information
`/name/<nameID>`

Provides more detailed information on the actor/actress, which as of now includes only movies and TV series.

Additional boolean parameters: `show_roles`, `show_titles`, `show_details`


### Six Degrees of Separation
`/bacon/name/<nameID>`

Perform a breadth first search to find the shortest path from the actor/actress to Kevin Bacon.  There is randomization of the possible paths to take, so repeating a search can yield a different path (but would still be the shortest path).

Additional parameters: `target_id` - specifies a different target actor/actress


`/bacon/title/<titleID>`

Similar to the above mode, but the first link in the path will be from the given title.  As a result, this isn't guaranteed to be the shortest path to Kevin Bacon.

Additional parameters: `target_id` - specifies a different target actor/actress


`/bacon/random`

Operates in title mode and starts with a "randomly" selected title.  There is some bias in the randomness so that the title isn't always obscure.

Additional parameters: `target_id` - specifies a different target actor/actress


### Examples

#### Searching for a title
```
lchu@lchu-ud:~/bacon$ curl -X GET -G http://localhost:9999/search/title --data-urlencode "q=full metal panic"|jq .
[
  {
    "cast": [
      {
        "nameID": "nm0782840",
        "name": "Tomokazu Seki"
      },
      {
        "nameID": "nm0950830",
        "name": "Satsuki Yukino"
      },
      {
        "nameID": "nm0633989",
        "name": "Yukana Nogami"
      }
    ],
    "type": "tvSeries",
    "startYear": "2018",
    "titleID": "tt7246908",
    "title": "Full Metal Panic! Invisible Victory"
  },
  {
    "cast": [
      {
        "nameID": "nm0782840",
        "name": "Tomokazu Seki"
      },
      {
        "nameID": "nm0950830",
        "name": "Satsuki Yukino"
      },
      {
        "nameID": "nm0235103",
        "name": "Jason Douglas"
      },
      {
        "nameID": "nm1327299",
        "name": "Ikue Kimura"
      },
      {
        "nameID": "nm0849050",
        "name": "Rie Tanaka"
      },
      {
        "nameID": "nm0160049",
        "name": "Luci Christian"
      },
      {
        "nameID": "nm0666547",
        "name": "Chris Patton"
      },
      {
        "nameID": "nm0722676",
        "name": "Monica Rial"
      },
      {
        "nameID": "nm0605451",
        "name": "Toshiyuki Morikawa"
      },
      {
        "nameID": "nm1145982",
        "name": "Mamiko Noto"
      }
    ],
    "type": "tvSeries",
    "startYear": "2003",
    "titleID": "tt0472436",
    "title": "Full Metal Panic? Fumoffu"
  },
  {
    "cast": [
      {
        "nameID": "nm0950830",
        "name": "Satsuki Yukino"
      },
      {
        "nameID": "nm0782840",
        "name": "Tomokazu Seki"
      },
      {
        "nameID": "nm0235103",
        "name": "Jason Douglas"
      },
      {
        "nameID": "nm0633989",
        "name": "Yukana Nogami"
      },
      {
        "nameID": "nm0960033",
        "name": "Akio Ôtsuka"
      },
      {
        "nameID": "nm0586528",
        "name": "Shin'ichirô Miki"
      },
      {
        "nameID": "nm0628649",
        "name": "Michiko Neya"
      },
      {
        "nameID": "nm1091445",
        "name": "Masahiko Tanaka"
      },
      {
        "nameID": "nm0632770",
        "name": "Tomomichi Nishimura"
      },
      {
        "nameID": "nm1327299",
        "name": "Ikue Kimura"
      }
    ],
    "type": "tvSeries",
    "startYear": "2002",
    "titleID": "tt0328739",
    "title": "Full Metal Panic!"
  },
  {
    "cast": [
      {
        "nameID": "nm0782840",
        "name": "Tomokazu Seki"
      },
      {
        "nameID": "nm0950830",
        "name": "Satsuki Yukino"
      },
      {
        "nameID": "nm0960033",
        "name": "Akio Ôtsuka"
      },
      {
        "nameID": "nm0586528",
        "name": "Shin'ichirô Miki"
      },
      {
        "nameID": "nm0628649",
        "name": "Michiko Neya"
      },
      {
        "nameID": "nm0633989",
        "name": "Yukana Nogami"
      },
      {
        "nameID": "nm0960037",
        "name": "Hôchû Ôtsuka"
      },
      {
        "nameID": "nm0632770",
        "name": "Tomomichi Nishimura"
      },
      {
        "nameID": "nm1327299",
        "name": "Ikue Kimura"
      },
      {
        "nameID": "nm0793987",
        "name": "Emi Shinohara"
      }
    ],
    "type": "tvSeries",
    "startYear": "2005",
    "titleID": "tt0451991",
    "title": "Full Metal Panic! The Second Raid"
  }
]
```

#### Searching for a name
The nameID can be used with endpoints such as `/bacon/name/<nameID>` and `/name/<nameID>`.

```
lchu@lchu-ud:~/bacon$ curl -s -X GET -G "http://localhost:9999/search/name" --data-urlencode "q=chris pratt"|jq .
[
  {
    "birthYear": "1979",
    "titles": [
      {
        "endYear": "2006",
        "type": "tvSeries",
        "startYear": "2002",
        "titleID": "tt0318883",
        "title": "Everwood"
      },
      {
        "type": "movie",
        "startYear": "2003",
        "titleID": "tt0295249",
        "title": "The Extreme Team"
      },
      {
        "endYear": "2015",
        "type": "tvSeries",
        "startYear": "2009",
        "titleID": "tt1266020",
        "title": "Parks and Recreation"
      },
      {
        "type": "movie",
        "startYear": "2009",
        "titleID": "tt1078885",
        "title": "Deep in the Valley"
      },
      {
        "type": "movie",
        "startYear": "2011",
        "titleID": "tt1715873",
        "title": "10 Years"
      },
      {
        "type": "movie",
        "startYear": "2012",
        "titleID": "tt5850902",
        "title": "On the Mat"
      },
      {
        "type": "movie",
        "startYear": "2012",
        "titleID": "tt1790885",
        "title": "Zero Dark Thirty"
      },
      {
        "type": "movie",
        "startYear": "2012",
        "titleID": "tt1195478",
        "title": "The Five-Year Engagement"
      },
      {
        "type": "movie",
        "startYear": "2013",
        "titleID": "tt2387559",
        "title": "Delivery Man"
      },
      {
        "type": "movie",
        "startYear": "2014",
        "titleID": "tt2015381",
        "title": "Guardians of the Galaxy"
      },
      {
        "type": "movie",
        "startYear": "2014",
        "titleID": "tt1490017",
        "title": "The Lego Movie"
      },
      {
        "type": "movie",
        "startYear": "2015",
        "titleID": "tt0369610",
        "title": "Jurassic World"
      },
      {
        "type": "movie",
        "startYear": "2016",
        "titleID": "tt1355644",
        "title": "Passengers"
      },
      {
        "type": "movie",
        "startYear": "2016",
        "titleID": "tt2404435",
        "title": "The Magnificent Seven"
      },
      {
        "type": "movie",
        "startYear": "2017",
        "titleID": "tt3896198",
        "title": "Guardians of the Galaxy Vol. 2"
      },
      {
        "type": "movie",
        "startYear": "2018",
        "titleID": "tt4881806",
        "title": "Jurassic World: Fallen Kingdom"
      },
      {
        "type": "movie",
        "startYear": "2019",
        "titleID": "tt1801045",
        "title": "Cowboy Ninja Viking"
      }
    ],
    "nameID": "nm0695435",
    "name": "Chris Pratt"
  },
  {
    "titles": [
      {
        "type": "movie",
        "startYear": "2006",
        "titleID": "tt0495158",
        "title": "Motorcycle"
      }
    ],
    "nameID": "nm2212645",
    "name": "Chris Pratt"
  },
  {
    "titles": [],
    "nameID": "nm9321372",
    "name": "Chris Pratt"
  }
]
```

#### Playing Six Degrees of Kevin Bacon
```
lchu@lchu-ud:~/bacon$ curl -s http://localhost:9999/bacon/random |jq .
[
  {
    "title": {
      "type": "movie",
      "startYear": "2017",
      "titleID": "tt3896198",
      "title": "Guardians of the Galaxy Vol. 2"
    },
    "from": {
      "role": [
        "Baby Groot"
      ],
      "nameID": "nm0004874",
      "name": "Vin Diesel"
    },
    "to": {
      "role": [
        "Gamora"
      ],
      "nameID": "nm0757855",
      "name": "Zoe Saldana"
    }
  },
  {
    "title": {
      "type": "movie",
      "startYear": "2004",
      "titleID": "tt0386504",
      "title": "Haven"
    },
    "from": {
      "role": [
        "Andrea"
      ],
      "nameID": "nm0757855",
      "name": "Zoe Saldana"
    },
    "to": {
      "role": [
        "Carl Ridley"
      ],
      "nameID": "nm0000200",
      "name": "Bill Paxton"
    }
  },
  {
    "title": {
      "type": "movie",
      "startYear": "1995",
      "titleID": "tt0112384",
      "title": "Apollo 13"
    },
    "from": {
      "role": [
        "Fred Haise"
      ],
      "nameID": "nm0000200",
      "name": "Bill Paxton"
    },
    "to": {
      "role": [
        "Jack Swigert"
      ],
      "nameID": "nm0000102",
      "name": "Kevin Bacon"
    }
  }
]
```

#### Looking up an actress
```
lchu@lchu-ud:~/bacon$ curl -s http://localhost:9999/name/nm3592338|jq .
{
  "birthYear": "1986",
  "titles": [
    {
      "role": [
        "Daenerys Targaryen"
      ],
      "type": "tvSeries",
      "startYear": "2011",
      "titleID": "tt0944947",
      "title": "Game of Thrones"
    },
    {
      "role": [
        "Evelyn"
      ],
      "type": "movie",
      "startYear": "2013",
      "titleID": "tt2402105",
      "title": "Dom Hemingway"
    },
    {
      "role": [
        "Sarah Connor"
      ],
      "type": "movie",
      "startYear": "2015",
      "titleID": "tt1340138",
      "title": "Terminator Genisys"
    },
    {
      "role": [
        "Lou Clark"
      ],
      "type": "movie",
      "startYear": "2016",
      "titleID": "tt2674426",
      "title": "Me Before You"
    },
    {
      "role": [
        "Susan Smith"
      ],
      "type": "movie",
      "startYear": "2017",
      "titleID": "tt5688068",
      "title": "Above Suspicion"
    },
    {
      "role": [
        "Verena"
      ],
      "type": "movie",
      "startYear": "2017",
      "titleID": "tt1544608",
      "title": "Voice from the Stone"
    },
    {
      "role": [
        "Qi'Ra"
      ],
      "type": "movie",
      "startYear": "2018",
      "titleID": "tt3778644",
      "title": "Solo: A Star Wars Story"
    }
  ],
  "nameID": "nm3592338",
  "name": "Emilia Clarke"
}
```
