#!/usr/bin/python
import gzip
import sys
import re
import urllib
import threading
import collections
import time
import json
import random
from sets import Set
from ast import literal_eval
from flask import Flask, request, jsonify, abort
from flask_api import status

flask_app = Flask(__name__)

#
# Constants to save memory
#
TITLE_TYPE_MOVIE = 0x1
TITLE_TYPE_TVSERIES = 0x2
TITLE_TYPE_UNKNOWN = 0x4

def decode_title_type(title_type):
    if title_type == TITLE_TYPE_MOVIE:
        return "movie"
    elif title_type == TITLE_TYPE_TVSERIES:
        return "tvSeries"
    else:
        return "Unknown ({})".format(title_type)

#
# Bacon numbers
# PATH
# /bacon/title/ttxxxxx
# /bacon/random
# /bacon/name/nmxxxxx
# title <movie/tv title> - This mode doesn't guarantee the shortest path to Kevin
#                          Bacon.  It's used mainly to show the path to Kevin Bacon
#                          starting with a link from this title.
# random - Uses the title mode and picks a random person from a random
#          title.
# name <actor>
#
# /title/txxxxx - Get details on a title
# /name/nmxxxxx - Get details on a name
# POST /name/match
# POST /title/match
# FIXME
# Can we avoid the /title and /name paths and figure it out ourselves?
# Weird cases are one name celebrities with movies of the same name
#
# tt0009860       movie   Adele   Adele   0       1919    \N      \N      Drama
# nm2233157       Adele   1988    \N      soundtrack,actress,composer     tt1074638,tt2304933,tt3521132,tt1464540
#
# tt3820142 movie   Drake   Drake   0   2018    \N  \N  Adventure,Comedy,Drama
# nm1013044       Drake   1986    \N      actor,soundtrack,composer       tt0423977,tt1667889,tt0288937,tt1229340
#
# Other
# PATH
# /cast/ttxxxxx
# cast <title>
#
# PATH
# /status
#
# 1 Thread to load Titles
# 1 Thread to load Name
#
# tt5311514	movie	Your Name	Kimi no na wa.	0	2016	\N	106	Animation,Drama,Fantasy
# tt7456298	movie	Your Name	Your Name	0	\N	\N	\N	Drama
# FIXME tt7456298 has no principals listed.  Remove these types
#
# Prioritize based on presence of info?
# Prioritize movies over tv
#
# lchu@lchu-ud:~/Downloads$ zgrep "Thomas F" name.basics.tsv.gz |grep Wilson
# nm0001855	Thomas F. Wilson	1959	\N	actor,writer,producer	tt0088763,tt2404463,tt0096874,tt0099088
# lchu@lchu-ud:~/Downloads$ zgrep tt0088763 title.principals.tsv.gz 
# tt0088763	10	nm0004637	editor	\N	\N
# tt0088763	1	nm0000150	actor	\N	["Marty McFly"]
# tt0088763	2	nm0000502	actor	\N	["Dr. Emmett Brown"]
# tt0088763	3	nm0000670	actress	\N	["Lorraine Baines"]
# tt0088763	4	nm0000417	actor	\N	["George McFly"]
# tt0088763	5	nm0000709	director	\N	\N
# tt0088763	6	nm0301826	writer	written by	\N
# tt0088763	7	nm0134635	producer	producer	\N
# tt0088763	8	nm0006293	composer	\N	\N
# tt0088763	9	nm0005678	cinematographer	director of photography	\N
#
# A person might not be listed as a principal, but the title can show up in the roles they're known for.
# The opposite is also true (see Ernie Reyes Jr. and Surf Ninjas)
#

class Config(object):
    def __init__(self):

        #
        # Include the following categories in a principal role.
        #
        self.allowed_name_categories = {'actor', 'actress', 'self'}

#        self.allowed_name_professions = {"actor", "actress"}
        self.allowed_name_professions = self.allowed_name_categories

        #
        # Genres found: [u'Action', u'Adult', u'Adventure', u'Animation', u'Biography', u'Comedy', u'Crime', u'Documentary', u'Drama', u'Family', u'Fantasy', u'Film-Noir', u'Game-Show', u'History', u'Horror', u'Music', u'Musical', u'Mystery', u'News', u'Reality-TV', u'Romance', u'Sci-Fi', u'Short', u'Sport', u'Talk-Show', u'Thriller', u'War', u'Western']
        #
        # Ignore the following for now:
        # ['Adult', 'Biography', 'Documentary', 'Game-Show', 'History', 'Music', 'News', 'Reality-TV', 'Short', 'Sport', 'Talk-Show', 'War']
        #
        # If a title includes a genre listed below, then it'll be included regardless.
        # ex:
        # tt0106770 movie   Dragon: The Bruce Lee Story Dragon: The Bruce Lee Story 0   1993    \N  120 Action,Biography,Drama
        #
        self.allowed_genres = {'Action', 'Adventure', 'Animation', 'Comedy', 'Crime', 'Drama', 'Family', 'Fantasy', 'Film-Noir', 'Horror', 'Musical', 'Mystery', 'Romance', 'Sci-Fi', 'Thriller', 'Western'}

        #
        # Talk shows are filtered out because two people who appeared on
        # a show weren't necessarily on there at the same time.
        #
        self.disallowed_genres = {'Game-Show', 'News', 'Reality-TV', 'Talk-Show'}

        #
        # Only care about movies and TV series for now.  Adding more types would require more memory to store the larger graph.
        #
        self.allowed_title_types = {'movie', 'tvSeries'}

        #
        # Randomize the path to the target ID.  The shortest path would still
        # be returned, but the vertices could be different for the same query.
        #
        self.randomize_path = True

        #
        # Enable to allow the REST server to honor a different target from
        # the default_target_id.  An obscure target could take a really long
        # time to return an answer, though.
        #
        self.allow_custom_target_id = False

        #
        # Kevin Bacon
        #
        self.default_target_id = "nm0000102"

        #
        # The port to listen to for REST requests
        #
        self.listen_port = 9999

        #
        # When using the random option, we can bias towards some pre-defined
        # suggestions so that the titles are not entirely obscure.  Good for
        # generating interest/discussion.
        #
        self.bias_towards_suggestions = 50

        #
        # Limit the number of non-principals to show.
        #
        self.max_non_principals_to_show = 5

class Title(object):
    def __init__(self, title_type, primary_title, start_year, end_year, genres):
        self.title_type = TITLE_TYPE_UNKNOWN
        if title_type == "tvSeries":
            self.title_type = TITLE_TYPE_TVSERIES
        elif title_type == "movie":
            self.title_type = TITLE_TYPE_MOVIE

        self.primary_title = primary_title
        self.start_year = start_year
        self.end_year = end_year

        #
        # Comment out to save memory
        #
#        self.genres = genres
        self.actors = {}

        #
        # Thomas F Wilson isn't a principal in BTTF, but he is known
        # for his role as Biff and would appear as a non-principal. When
        # moving from actor to actor in the BFS, we can optionally consider the
        # entries here.
        #
        self.non_principals = Set()

    def __str__(self):
        actors = []
        for x in self.actors:
            actors.append("{} ({})".format(x, self.actors[x]))

        return "title_type = {}, primary_title = {}, start_year = {}, actors = {}".format(
            self.title_type,
            self.primary_title,
            self.start_year,
#            self.genres,
            ", ".join(actors)
        )

class Role(object):
    def __init__(self, ordering=None, category=None, characters=None):
        #
        # Comment out to save memory
        #
#        self.category = category
        self.characters = None
        self.ordering = None
        if ordering:
            self.ordering = int(ordering)

        if characters:
            self.characters = literal_eval(characters)


#    def __str__(self):
#        return "category = {}, characters = {}".format(
#            self.category,
#            self.characters,
#        )

class Edge(object):
    def __init__(self, title_id, actor_id):
        self.title_id = title_id
        self.actor_id = actor_id

    def __str__(self):
        return "title_id = {}, actor_id = {}".format(
            self.title_id,
            self.actor_id
        )

class Name(object):
    def __init__ (
        self,
        primary_name,
        birth_year,
        death_year,
        primary_profession,
        titles
    ):
        self.primary_name = primary_name
        self.birth_year = birth_year
        self.death_year = death_year
        #
        # Comment out to save memory
        #
#        self.primary_profession = primary_profession

        #
        # Contains principal roles and non-principal roles.
        # The former is from titles.principals, and the latter
        # is from name.basics
        #
        self.titles = Set(titles)

    def add_title(self, title_id=None):
        if title_id:
            self.titles.add(title_id)

    def __str__(self):
        return "primary_name = {}, titles = {}".format(self.primary_name, self.titles)


def clean_input(line):
    cleaned_up = []
    for x in line.split("\t"):
        x = x.rstrip()
        if x == "\\N":
            x = None
        cleaned_up.append(x)

    return cleaned_up

class Names(object):
    def __init__(
        self,
        config,
        name_basics_filename="name.basics.tsv.gz",
        title_obj=None
    ):
        print "Loading {}...".format(name_basics_filename)

        self.allowed_professions = config.allowed_name_professions
        self.names = {}

        #
        # Resolve a name to nconst
        #
        self.names_to_id = {}

        self.add_non_principals = True

        #
        # Load the name.basics file.  Skip over titles that don't exist in the Titles
        # object, because we only want to track movies and tvSeries
        #
        with gzip.open(name_basics_filename, 'rb') as f:
            for line in f:
                #
                # nconst    primaryName birthYear   deathYear   primaryProfession   knownForTitles
                # nm0001705   Rob Schneider   1963    \N  actor,writer,miscellaneous  tt0343660,tt0302640,tt0367652,tt0205000
                #
                cleaned_up = clean_input(line.decode('utf-8'))
                nconst, primary_name, birth_year, death_year, primary_profession, known_for_titles = cleaned_up
                if nconst == "nconst": # Skip the header
                    continue

                if known_for_titles:
                    known_for_titles = known_for_titles.split(",")
                    known_for_titles = [k for k in known_for_titles if k in title_obj.titles]

                if primary_profession:
                    primary_profession = primary_profession.split(",")

                process_this_actor = False
                for x in self.allowed_professions:
                    if x in primary_profession:
                        process_this_actor = True

                if process_this_actor:
                    if nconst not in self.names:
                        self.names[nconst] = Name(
                            primary_name,
                            birth_year,
                            death_year,
                            primary_profession,
                            known_for_titles
                        )

                    #
                    # The title.principals.tsv.gz contains more titles
                    # that a person has performed in.  We cached these earlier
                    # and can add them to this person now.
                    #
                    if (
                        title_obj.temp_actor_to_titles and
                        nconst in title_obj.temp_actor_to_titles
                    ):
                        for x in title_obj.temp_actor_to_titles[nconst]:
                            self.names[nconst].add_title(x)

                    if known_for_titles:
                        for x in known_for_titles:
                            self.names[nconst].add_title(x)
                            #
                            # This file contains the actor's roles, which could
                            # be many.
                            #
                            if self.add_non_principals:
                                title_obj.add_non_principal(title_id=x, actor_id=nconst)

                    #
                    # This pruning saves 1.5GB of memory
                    #
                    #  BEFORE
                    #  29554 lchu      20   0 7839800 7.440g   6964 S   0.3 47.8   4:19.50 python
                    #
                    #  AFTER
                    #  31278 lchu      20   0 6325512 5.996g   6916 S   0.0 38.5   3:59.62 python
                    #
                    if len(self.names[nconst].titles) == 0:
                        self.names.pop(nconst)
                        continue

                    #
                    # Since we could have more than one match,
                    # names_to_id returns an array
                    #
                    name_lcase = primary_name.lower()
                    if name_lcase not in self.names_to_id:
                        self.names_to_id[name_lcase] = []
                    self.names_to_id[name_lcase].append(nconst)

        #
        # Free memory taken up by this thing
        #
        title_obj.temp_actor_to_titles = None

        print "Loaded {} names".format(len(self.names.keys()))

        #
        # Convert from a Set to a simple list, but sorted by the year
        # of the title.
        #
        print "Sorting titles within each actor..."
        for x in self.names:
            temp_titles = sorted(
                self.names[x].titles,
                key=lambda t: title_obj.titles[t].start_year
            )
            self.names[x].titles = temp_titles

    def get_name_by_id(self, nconst):
        if nconst not in self.names:
            return None
        return self.names[nconst].primary_name

    def get_name_id(self, name):
        name_lcase = name.lower()

        if name_lcase in self.names_to_id:
            result = self.names_to_id[name_lcase]
            return result

        return None

    def get_name_by_name(self, name):
        name_lcase = name.lower()

        if name_lcase in self.names_to_id:
            result = self.names_to_id[name_lcase]
            return result

        return []


class Titles(object):

    def __init__(
        self,
        config,
        basics_filename="title.basics.tsv.gz",
        principals_filename="title.principals.tsv.gz",
    ):
        print "Loading {}...".format(basics_filename)

        #
        # Hash a titleID to a full movie name
        #
        self.titles = {}

        self.titles_to_id = {}

        self.allowed_types = config.allowed_title_types
        self.allowed_genres = config.allowed_genres
        self.disallowed_genres = config.disallowed_genres

        #
        # Temporary
        #
        self.temp_actor_to_titles = {}

        all_possible_genres = Set()

        with gzip.open(basics_filename, 'rb') as f:
            for line in f:
                cleaned_up = clean_input(line.decode('utf-8'))
                tconst, title_type, primary_title, original_title, is_adult, start_year, end_year, runtime, genres = cleaned_up

                #
                # Skip the header
                #
                if tconst == "tconst":
                    continue

                genre_allowed = False
                disallowed = False
                if genres:
                    genres = genres.split(",")
                    for x in genres:
                        all_possible_genres.add(x)
                        if x in self.allowed_genres:
                            genre_allowed = True
                        elif x in self.disallowed_genres:
                            disallowed = True

                if (
                    title_type not in self.allowed_types or
                    not start_year or
                    not genre_allowed or
                    disallowed
                ):
                    continue

                if tconst not in self.titles:
                    self.titles[tconst] = Title(
                        title_type,
                        primary_title,
                        start_year,
                        end_year,
                        genres
                    )

                #
                # Since we could have more than one match for a title
                # (ie. The Avengers), titles_to_id returns an array
                #
                title_lcase = primary_title.lower()
                if title_lcase not in self.titles_to_id:
                    self.titles_to_id[title_lcase] = []
                self.titles_to_id[title_lcase].append(tconst)

                #
                # Allow searches via an alternate name.  Useful for
                # Japanese titles
                #
                if original_title and original_title.lower() != title_lcase:
                    title_lcase = original_title.lower()
                    if title_lcase not in self.titles_to_id:
                        self.titles_to_id[title_lcase] = []
                    self.titles_to_id[title_lcase].append(tconst)
        print u"Genres found: {}".format(sorted(all_possible_genres))

        #
        # This is used to allow us to pick a random actor from this
        # title and link him/her to Kevin Bacon.  It's not needed
        # if a specific actor is provided.  I suppose it also
        # provides supplementary info on a person's role in a title.
        #
        self.process_actors = True
        if self.process_actors:
            print "Loading {}...".format(principals_filename)
            self.allowed_categories = config.allowed_name_categories
            with gzip.open(principals_filename, 'rb') as f:
                for line in f:
                    cleaned_up = clean_input(line.decode('utf-8'))
                    tconst, ordering, nconst, category, job, characters = cleaned_up

                    if (
                        category not in self.allowed_categories or
                        tconst == "tconst" # Skip the header
                    ):
                        continue

                    if tconst in self.titles:
                        self.titles[tconst].actors[nconst] = Role(ordering, category, characters)

                        if nconst not in self.temp_actor_to_titles:
                            self.temp_actor_to_titles[nconst] = Set()
                        self.temp_actor_to_titles[nconst].add(tconst)

        #
        # Preserve the billing order
        #
        print "Sorting billing order within each title..."
        for x in self.titles:
            temp_actors = collections.OrderedDict(
                sorted(
                    self.titles[x].actors.items(),
                    key=lambda t: t[1].ordering
                )
            )
            self.titles[x].actors = temp_actors
        print "Loaded {} titles".format(len(self.titles_to_id.keys()))


    def get_role(self, title_id, actor_id):
        if title_id not in self.titles:
            raise Exception("Unknown titleID {}".format(title_id))

        if actor_id not in self.titles[title_id].actors:
            return None
        return self.titles[title_id].actors[actor_id]

    def get_list_of_matches_to_try(self, title):
        #
        # 1) Normal match
        # 2) Substitute out 'and' for '&'
        # 3) Substitute out '&' for 'and'
        #
        title_lcase = title.lower()
        match_attempts = ([
            title_lcase,
            re.sub(r'\band\b', '&', title_lcase),
            re.sub('&', 'and', title_lcase)
        ])
        return match_attempts

    def starts_with(self, title):
        #
        # titles_to_id is also a list
        #
        for x in self.get_list_of_matches_to_try(title):
            list_of_lists = [self.titles_to_id[k] for k in self.titles_to_id.keys() if k.startswith(x)]
            result = [val for sublist in list_of_lists for val in sublist]
            if result:
                #
                # titles_to_id also contains alternate search strings (anime
                # english titles, for instance).  Remove the duplicates that
                # can result from this.
                #
                return list(Set(result))

        return []

    def get_title_by_id(self, title_id):
        if title_id not in self.titles:
            return None

        return self.titles[title_id]

    def get_title_by_name(self, title, year=None):
        title_lcase = title.lower()
        for x in self.get_list_of_matches_to_try(title):
            if x in self.titles_to_id:
                #FIXME
                result = self.titles_to_id[x]
                if year:
                    result = [k for k in result if self.titles[k].start_year == year]
                return result

        return None

    def get_actors(self, title_id):
        if title_id not in self.titles:
            raise Exception("Unknown titleID {}".format(title_id))

        return self.titles[title_id].actors

    def get_non_principals(self, title_id):
        if title_id not in self.titles:
            raise Exception("Unknown titleID {}".format(title_id))

        return list(self.titles[title_id].non_principals)

    def add_non_principal(self, title_id=None, actor_id=None):
        if title_id not in self.titles:
            raise Exception("Unknown titleID {}".format(title_id))

        if (
            actor_id not in self.titles[title_id].non_principals and
            actor_id not in self.titles[title_id].actors
        ):
            self.titles[title_id].non_principals.add(actor_id)

class Solution(object):
    def __init__(
        self,
        config,
        titles,
        names,
    ):
        self.default_target_id = config.default_target_id
        self.titles = titles
        self.names = names
        self.randomize = config.randomize_path
        self.bias_towards_suggestions = config.bias_towards_suggestions

        self.tv_titles = (
            [
                "tt5420376", # Riverdale
                "tt0475784", # Westworld
                "tt0944947", # Game of Thrones
                "tt2261227", # Altered Carbon
                "tt4016454", # Supergirl
                "tt0182576", # Family Guy
                "tt0096697", # The Simpsons
                "tt0098904", # Seinfeld
                "tt0106179", # The X-Files
                "tt0098800", # Fresh Prince
                "tt0118276", # Buffy
                "tt0083399", # Cheers
                "tt0108778", # Friends
                "tt0096694", # SBTB
                "tt0106179", # The X-Files
                "tt0092400", # Married With Children
                "tt0096657", # Mr. Bean
                "tt0081873", # Hill Street Blues
                "tt0072582", # Welcome Back, Kotter
                "tt0077089", # Taxi
                "tt0083437", # Knight Rider
                "tt0096542", # Baywatch
                #
                # 80s/90s Cartoons/Kids shows
                #
                "tt0074028", # The Muppet Show
                "tt0088528", # Adventures of the Gummi Bears
                "tt0086764", # Muppet Babies
                "tt0131613", # TMNT (80s)
                "tt0092345", # DuckTales
                "tt0088631", # Thundercats
                "tt0096557", # Rescue Rangers
                "tt0101076", # Darkwing Duck
                "tt0098929", # Tiny Toon Adventures
                "tt0084972", # Alvin and the Chipmunks
                "tt0088563", # MASK
                "tt0090506", # The Real Ghostbusters
                "tt0124257", # Slimer! and the Real Ghostbusters
                "tt0085011", # Dungeons & Dragons
                "tt0086719", # G.I. Joe
                "tt0163438", # C.O.P.S
                "tt0086817", # The Transformers
                "tt0085033", # Inspector Gadget
                "tt0189315", # Beast Machines: Transformers
                "tt0115108", # Beast Wars: Transformers
                "tt0236893", # Centurions
                "tt0094469", # Garfield and Friends
                "tt0092476", # Visionaries
                "tt0090461", # Jem
                "tt0284713", # The Care Bears Family
                "tt0098924", # TaleSpin
                "tt0096707", # The Super Mario Bros. Super Show!
                "tt0088643", # Wuzzles
                "tt0174420", # Popples
                "tt0096554", # Captain N
                "tt0090390", # ALF
                "tt0081848", # Danger Mouse
                "tt0088500", # Count Duckula
                "tt0122811", # Teddy Ruxpin
                "tt0085008", # Saturday Supercade
                "tt0211171", # Turbo-Teen
                "tt0126158", # He-Man
                "tt0081933", # Smurfs
                "tt0101178", # Ren and Stimpy
                "tt0103359", # Batman: The Animated Series
                "tt0103584", # X-Men
                "tt0105941", # Animaniacs
                "tt0105950", # Beavis and Butthead
                #
                # Canadiana
                #
                "tt0135735", # Rocket Robin Hood
                "tt0271917", # The Mighty Hercules
                "tt0189265", # The Friendly Giant
                "tt0199249", # Mr. Dressup
                "tt0108903", # ReBoot
                "tt0078714", # You Can't Do That On Television
                "tt0103352", # Are You Afraid of the Dark?
                "tt0111987", # Goosebumps
                #
                # Anime
                #
                "tt0877057", # Death Note (2006)
                "tt2560140", # Shingeki no Kyojin
                "tt2250192", # Sword Art Online
                "tt1639109", # Angel Beats!
                "tt4508902", # One Punch Man
                "tt1910272", # Steins;Gate
                "tt3741634", # Tokyo Ghoul
                "tt0994314", # Code Geass: Hangyaku no Lelouch
                "tt0409591", # Naruto
                "tt5311514", # Your Name
                "tt0119698", # Princess Mononoke
                "tt0245429", # Spirited Away
                "tt0121220", # DBZ
                "tt0094625", # Anime
            ]
        )
        self.oscar_titles = (
            [
                #
                # 2017 Oscar nominated movies
                #
                "tt5726616", # CMBYN
                "tt5580036", # I, Tonya
                "tt5649144", # The Florida Project
                "tt1856101", # Blade Runner 2049
                "tt5052448", # Get Out
                "tt5027774", # Three Billboards
                "tt4925292", # Lady Bird
                "tt5580390", # The Shape of Water
                "tt5013056", # Dunkirk
                "tt7002100", # Coco
                "tt5639354", # A Fantastic Woman (foreign language film)
                "tt7048622", # The Insult (foreign language film)
                "tt6304162", # Loveless (foreign language film)
                "tt5607714", # On Body and Soul (foreign language film)
                "tt4995790", # The Square (foreign language film)
                "tt3874544", # The Boss Baby
                "tt3411444", # Ferdinand
                "tt3901826", # The Breadwinner
                "tt3262342", # Loving Vincent
                "tt4209788", # Molly's Game
                "tt3315342", # Logan
                "tt3521126", # The Disaster Artist
                "tt5294550", # All the Money in the World
                "tt2396589", # Mudbound
                "tt5776858", # Phantom Thread
                "tt4555426", # Darkest Hour
                "tt6294822", # The Post
                "tt6000478", # Roman J. Israel, Esq.
                "tt2527336", # Star Wars: The Last Jedi
                "tt5301662", # Marshall
                "tt1485796", # The Greatest Showman
                "tt3890160", # Baby Driver
                "tt2771200", # Beauty and the Beast
                "tt5816682", # Victoria and Abdul
                "tt2543472", # Wonder
                "tt3896198", # Guardians of the Galaxy 2
                "tt3731562", # Kong: Skull Island
                "tt3450958", # War for the Planet of the Apes
                #
                # 2016 Oscar nominated movies
                #
                "tt4975722", # Moonlight
                "tt2543164", # Arrival
                "tt2671706", # Fences
                "tt2119532", # Hacksaw Ridge
                "tt2582782", # Hell or High Water
                "tt4846340", # Hidden Figures
                "tt3783958", # La La Land
                "tt3741834", # Lion
                "tt4034228", # Manchester by the Sea
                "tt3553976", # Captain Fantastic
                "tt3716530", # Elle
                "tt4669986", # Loving
                "tt1619029", # Jackie
                "tt4136084", # Florence Foster Jenkins
                "tt4550098", # Nocturnal Animals
                "tt3464902", # The Lobster
                "tt4385888", # 20th Century Women
                "tt4302938", # Kubo and the Two Strings
                "tt2948356", # Zootopia
                "tt3521164", # Moana
                "tt2321405", # My Life as a Zucchini
                "tt3666024", # The Red Turtle
                "tt5186714", # The Salesman (foreign language film)
                "tt5186860", # Land of Mine (foreign language film)
                "tt4080728", # A Man Called Ove (foreign language film)
                "tt4239726", # Tanna (foreign language film)
                "tt4048272", # Toni Erdmann (foreign language film)
                "tt1355644", # Passengers
                "tt1679335", # Trolls
                "tt5278466", # Jim: The James Foley Story
                "tt1860357", # Deepwater Horizon
                "tt3263904", # Sully
                "tt3748528", # Rogue One
                "tt4172430", # 13 Hours: The Secret Soldiers of Benghazi
                "tt0490215", # Silence
                "tt3183660", # Fantastic Beasts
                "tt0475290", # Hail, Caesar
                "tt2660888", # Star Trek Beyond
                "tt3640424", # Allied
                "tt3040964", # The Jungle Book
                "tt1211837", # Doctor Strange
                #
                # 2015 Oscar nominated movies
                #
                "tt2179136", # American Sniper
                "tt2562232", # Birdman
                "tt1065073", # Boyhood
                "tt2278388", # The Grand Budapest Hotel
                "tt2084970", # The Imitation Game
                "tt1020072", # Selma
                "tt2980516", # The Theory of Everything
                "tt2582802", # Whiplash
                "tt2737050", # Two Days, One Night
                "tt3316960", # Still Alice
                "tt2267998", # Gone Girl
                "tt2305051", # Wild
                "tt1100089", # Foxcatcher
                "tt2180411", # Into the Woods
                "tt1872194", # The Judge
                "tt2245084", # Big Hero 6
                "tt0787474", # The Boxtrolls
                "tt1646971", # How to Train Your Dragon 2
                "tt1865505", # Song of the Sea
                "tt2576852", # The Tale of the Princess kaguya
                "tt2718492", # Ida (foreign language film)
                "tt2802154", # Leviathan (foreign language film) (more than one match in imdb)
                "tt2991224", # Tangerines (foreign language film)
                "tt3409392", # Timbuktu (foreign language film)
                "tt3011894", # Wild Tales (foreign language film)
                "tt2872718", # Nightcrawler
                "tt2015381", # Guardians of the Galaxy
                "tt0816692", # Interstellar
                "tt2473794", # Mr. Turner
                "tt1490017", # The Lego Movie
                "tt3125324", # Beyond the Lights
#                "tt2049586", # Glen Campbell (commented out because no cast)
                "tt1980929", # Begin Again (more than one match in imdb)
                "tt1809398", # Unbroken
                "tt1791528", # Inherent Vice
                "tt1587310", # Maleficent
                "tt2310332", # The Hobbit: The Battle of the Five Armies
                "tt1843866", # Captain America: The Winter Soldier
                "tt2103281", # Dawn of the Planet of the Apes
                "tt1877832", # X-Men: Days of Future Past
                #
                # Random stuff
                #
                "tt0092106", # Transformers animated movie
                "tt0147800", # 10 Things I Hate About You
                "tt0094737", # Big
                "tt0096928", # Bill and Ted
                "tt0090728", # Big Trouble in Little China
                "tt0074285", # Carrie
                "tt0109445", # Clerks
                "tt0109506", # The Crow
                "tt0070034", # Enter the Dragon
                "tt0080684", # Star Wars - The Empire Strikes Back
                "tt0107048", # Groundhog Day
            ]
        )

    def random(
        self,
        target_id=None,
        use_non_principals=False # to make this faster...
    ):
        #
        # Try up to this many times to find a random answer
        #
        max_attempts = 10

        for i in range(0, max_attempts):
            #
            # Pick a random title (rather than a random name), because there
            # are far fewer titles than names.
            #
            if self.bias_towards_suggestions and random.choice(range(0,100)) < self.bias_towards_suggestions:
                tconst = random.choice(self.oscar_titles + self.tv_titles)
            else:
                tconst = random.choice(self.titles.titles.keys())

            answer = self.solve_by_tconst(
                title_id=tconst,
                target_id=target_id,
                use_non_principals=use_non_principals
            )

            #
            # Break out of the loop if we got a non-empty answer
            #
            if answer:
                break

        return answer

    def solve_by_tconst(
        self,
        title_id,
        target_id=None,
        use_non_principals=False # Could flip this to true if we fail the first time with false
    ):
        name_list = list(self.titles.titles[title_id].actors)
        if use_non_principals:
            name_list += self.titles.get_non_principals(title_id)

        #
        # Filter out names that aren't actor, actress
        # TODO Remove titles with no actors up front.
        #
        name_list = [k for k in name_list if self.names.get_name_by_id(k)]

        #
        # Name_list could be empty...
        #
        if not name_list:
            return None

        if self.randomize:
            random.shuffle(name_list)

        #
        # FIXME.  There is an edge case.  Let's say there is one actor
        # in the entire cast that links to the target_id.  If we start
        # the BFS with this actor, then we'll never get to the target because
        # none of the other cast members has a link to the target.  As a
        # ghetto workaround, we'll do the search a second time with a different
        # cast member if the first attempt yields no path.
        #
        x = name_list[0]
#        for x in name_list[:2]:
        answer = self.solve_by_nconst(
            source_id=x,
            target_id=target_id,
            source_title_id=title_id,
            use_non_principals=use_non_principals
        )
        return answer

    def solve_by_nconst(
        self,
        source_id,
        target_id=None,
        source_title_id=None,
        use_non_principals=True,
        debug=False
    ):
        if source_id not in self.names.names:
            raise Exception("Unknown source_id {}".format(source_id))

        actor_queue = [source_id]
        answer = self.solve(
            actor_queue,
            target_id=target_id,
            source_title_id=source_title_id,
            use_non_principals=use_non_principals,
            debug=debug
        )
        return answer

    def solve(
        self,
        actor_queue,
        target_id=None,
        source_title_id=None,
        use_non_principals=False,
        max_principal_depth=2,
        debug=False
    ):
        found_path = False

        #
        # Seed our work queue
        #
        source_id = actor_queue[0]
        prev = {}
        prev[source_id] = Edge(None,None)
        principal_depth = 0
        if not target_id:
            target_id = self.default_target_id

        while len(actor_queue) > 0:
            current = actor_queue.pop(0)
            if current == target_id:
                found_path = True
                break

            #
            #
            #
            if current in self.names.names:
                if debug:
                    print "Current actor: {}".format(
                        self.names.get_name_by_id(current)
                    )
                #
                # Actor was in a title by themselves
                #
#                if len(self.names.names[current].titles) == 0:
#                    continue

                if source_title_id:
                    if debug:
                        print "Using source title: {}".format(
                            self.titles.get_title_by_id(source_title_id).primary_title
                        )
                    titles_list = [source_title_id]
                    source_title_id = None
                else:
                    # FIXME - this isn't really respecting the use_non_principals config
                    titles_list = list(self.names.names[current].titles)

                #
                # Note that shuffle works in place and mutates the
                # original data.  It's not a big deal for this app.
                #
                if self.randomize:
                    random.shuffle(titles_list)

                for t in titles_list:
                    actors_list = list(self.titles.get_actors(t))
                    if use_non_principals:
                        actors_list += self.titles.get_non_principals(t)

                    #
                    # Prune the list before randomizing it.  This prevents
                    # us from visiting an actor that's already in the BFS
                    #
                    actors_list = [k for k in actors_list if k not in prev]
                    if self.randomize:
                        random.shuffle(actors_list)
                    if debug:
                        print "Title: {}\nActors: {}".format(
                            self.titles.get_title_by_id(t).primary_title,
                            actors_list
                        )
                    for x in actors_list:
                        if x not in prev:
                            actor_queue.append(x)
                            prev[x] = Edge(title_id=t, actor_id=current)

                #
                # Limit the crazy fan-out
                #
                if use_non_principals:
                    principal_depth += 1
                    if principal_depth >= max_principal_depth:
                        use_non_principals = False

        if found_path:
            path = []
            current = target_id
            depth = 0
            output_array = []
            while current != source_id and depth < 32:
                nconst = current
                tconst = prev[current].title_id
                next_actor = prev[current].actor_id

                output_dict = {
                    'from': next_actor,
                    'to': nconst,
                    'title': tconst
                }
                output_array.append(output_dict)

                current = next_actor
                depth += 1

            output_array.reverse()
            return output_array
        else:
            #
            # Could happen due to the following restrictions
            # movies, tvSeries
            # actor, actress
            #
            return []

def debug_test(movie_ids, titles, names):
    for x in movie_ids:
        title = titles.get_title_by_id(x)
        print "{}".format(title)

        for i in title.actors:
            full_name = names.get_name_by_id(i)
            output_str = "{}".format(full_name)
            role_info = title.actors[i]
            if role_info and role_info.characters:
                output_str += " ({})".format(role_info.characters)
            print "{}".format(output_str)

class PrettyPrinter(object):
    def __init__(self, config, titles, names):
        self.config = config
        self.titles = titles
        self.names = names

    def name(self, actor_id, show_titles=False, show_roles=False, show_details=False):
        name_str = self.names.get_name_by_id(actor_id)
        output = {}
        if name_str:
            n = self.names.names[actor_id]
    #        self.primary_profession = primary_profession
            output = {
                'name': name_str,
                'nameID': actor_id,
            }

            if show_titles:
                titles_pretty = []
                for t in n.titles:
                    s = self.titles.get_title_by_id(t)
                    if s:
                        #
                        # show_cast must be False to avoid a recursive loop
                        #
                        output_t = self.title(title_id=t, show_cast=False)

                        if show_roles:
                            role = self.titles.get_role(title_id=t, actor_id=actor_id)
                            if role and role.characters:
                                output_t['role'] = role.characters

                        titles_pretty.append(output_t)

                output['titles'] = titles_pretty

            if show_details:
                if n.birth_year:
                    output['birthYear'] = n.birth_year
                if n.death_year:
                    output['deathYear'] = n.death_year

        return output

    def title(
        self,
        title_id,
        show_cast=True,
        show_roles=False,
        show_non_principals=False
    ):
        t = self.titles.get_title_by_id(title_id)

        output = {
            'title': t.primary_title,
            'titleID': title_id,
        }

        output['type'] = decode_title_type(t.title_type)
        if t.start_year:
            output['startYear'] = t.start_year
        if t.end_year:
            output['endYear'] = t.end_year
#            output['genres'] = t.genres

        def process_nconsts(work_queue, show_roles):
            actors_pretty = []
            for nconst in work_queue:
                pretty_name = self.names.get_name_by_id(nconst)
                if pretty_name:
                    #
                    # show_titles has to be false to prevent infinite recursion
                    #
                    a = self.name(nconst, show_titles=False, show_roles=False, show_details=False)
                    if show_roles:
                        role = self.titles.get_role(title_id=title_id, actor_id=nconst)
                        if role and role.characters:
                            a['role'] = role.characters
                    actors_pretty.append(a)
            return actors_pretty

        if show_cast:
            output['cast'] = process_nconsts(t.actors, show_roles)

            if show_non_principals:
                non_principals = self.titles.get_non_principals(title_id)

                temp_queue = []
                for x in non_principals:
                    if x not in t.actors:
                        temp_queue.append(x)
                output['nonPrincipalCast'] = process_nconsts(temp_queue, show_roles)

        return output

    def solution(self, answer):
        if not answer:
            return []

        output_str = []
        for link in answer:
            tconst = link['title']
            from_nconst = link['from']
            to_nconst = link['to']

            row_data = collections.OrderedDict()
            row_data['title'] = self.title(title_id=tconst, show_cast=False)

            row_data['from'] = self.name(actor_id=from_nconst)
            role = self.titles.get_role(title_id=tconst, actor_id=from_nconst)
            if role and role.characters:
                row_data['from']['role'] = role.characters

            row_data['to'] = self.name(actor_id=to_nconst)
            role = self.titles.get_role(title_id=tconst, actor_id=to_nconst)
            if role and role.characters:
                row_data['to']['role'] = role.characters

            output_str.append(row_data)

        return output_str

class App(object):
    def __init__(self):
        self.config = Config()
        self.titles = Titles(self.config)
        self.names = Names(self.config, title_obj=self.titles)
        self.solution = Solution(self.config, titles=self.titles, names=self.names)
        self.pretty_printer = PrettyPrinter(self.config, titles=self.titles, names=self.names)

    def stress_test(self):
        #
        # Stress test
        #
        for x in range(0,99):
            a = self.solution.random()
            for y in self.pretty_printer.solution(a):
                print u"{}".format(y)
            print u""

@flask_app.route("/bacon/title/<tconst>", methods=['GET'])
def get_bacon_title(tconst):
    if not flask_app.tconst_re.match(tconst):
        flask_app.errorMessage['error'] = u"Invalid title ID"
        return json.dumps(flask_app.errorMessage),status.HTTP_400_BAD_REQUEST, {'Content-Type': 'application/json'}

    target_id = request.args.get('target_id')

    if target_id and not flask_app.core.config.allow_custom_target_id:
        flask_app.errorMessage['error'] = u"Custom target ID is disabled"
        return json.dumps(flask_app.errorMessage),status.HTTP_400_BAD_REQUEST, {'Content-Type': 'application/json'}

    if target_id and not flask_app.nconst_re.match(target_id):
        flask_app.errorMessage['error'] = u"Invalid target ID"
        return json.dumps(flask_app.errorMessage),status.HTTP_400_BAD_REQUEST, {'Content-Type': 'application/json'}

    s = flask_app.core.solution.solve_by_tconst(tconst, target_id=target_id)

    if not s:
        return json.dumps([]), status.HTTP_200_OK, {'Content-Type': 'application/json'}

    answer_dict = flask_app.core.pretty_printer.solution(s)
    return json.dumps(answer_dict), status.HTTP_200_OK, {'Content-Type': 'application/json'}

@flask_app.route("/bacon/name/<nconst>", methods=['GET'])
def get_bacon_name(nconst):
    if not flask_app.nconst_re.match(nconst):
        flask_app.errorMessage['error'] = u"Invalid name ID"
        return json.dumps(flask_app.errorMessage),status.HTTP_400_BAD_REQUEST, {'Content-Type': 'application/json'}

    target_id = request.args.get('target_id')

    if target_id and not flask_app.core.config.allow_custom_target_id:
        flask_app.errorMessage['error'] = u"Custom target ID is disabled"
        return json.dumps(flask_app.errorMessage),status.HTTP_400_BAD_REQUEST, {'Content-Type': 'application/json'}

    if target_id and not flask_app.nconst_re.match(target_id):
        flask_app.errorMessage['error'] = u"Invalid target ID"
        return json.dumps(flask_app.errorMessage),status.HTTP_400_BAD_REQUEST, {'Content-Type': 'application/json'}

    s = flask_app.core.solution.solve_by_nconst(nconst, target_id=target_id)

    if not s:
        return json.dumps([]), status.HTTP_200_OK, {'Content-Type': 'application/json'}

    answer_dict = flask_app.core.pretty_printer.solution(s)
    return json.dumps(answer_dict), status.HTTP_200_OK, {'Content-Type': 'application/json'}

@flask_app.route("/bacon/random", methods=['GET'])
def get_bacon_random():

    target_id = request.args.get('target_id')

    if target_id and not flask_app.core.config.allow_custom_target_id:
        flask_app.errorMessage['error'] = u"Custom target ID is disabled"
        return json.dumps(flask_app.errorMessage),status.HTTP_400_BAD_REQUEST, {'Content-Type': 'application/json'}

    if target_id and not flask_app.nconst_re.match(target_id):
        flask_app.errorMessage['error'] = u"Invalid target ID"
        return json.dumps(flask_app.errorMessage),status.HTTP_400_BAD_REQUEST, {'Content-Type': 'application/json'}

    s = flask_app.core.solution.random(target_id=target_id)

    if not s:
        flask_app.errorMessage['error'] = u"Error generating solution"
        return json.dumps(flask_app.errorMessage),status.HTTP_500_INTERNAL_SERVER_ERROR, {'Content-Type': 'application/json'}

    answer_dict = flask_app.core.pretty_printer.solution(s)

    return json.dumps(answer_dict), status.HTTP_200_OK, {'Content-Type': 'application/json'}

@flask_app.route("/title/<tconst>", methods=['GET'])
def get_title(tconst):
    show_roles = False if request.args.get('show_roles') == 'false' else True
    show_cast = False if request.args.get('show_cast') == 'false' else True
    show_non_principals = True if request.args.get('show_non_principals') == 'true' else False

    if not flask_app.tconst_re.match(tconst):
        flask_app.errorMessage['error'] = u"Invalid title ID"
        return json.dumps(flask_app.errorMessage),status.HTTP_400_BAD_REQUEST, {'Content-Type': 'application/json'}

    #
    # Make sure the title exists.
    #
    result = flask_app.core.titles.get_title_by_id(tconst)
    if not result:
        abort(404)

    title_dict = flask_app.core.pretty_printer.title(
        tconst,
        show_cast=show_cast,
        show_roles=show_roles,
        show_non_principals=show_non_principals
    )
    return json.dumps(title_dict), status.HTTP_200_OK, {'Content-Type': 'application/json'}

@flask_app.route("/name/<nconst>", methods=['GET'])
def get_name(nconst):
    #
    show_roles = False if request.args.get('show_roles') == 'false' else True
    show_details = False if request.args.get('show_details') == 'false' else True
    show_titles = False if request.args.get('show_titles') == 'false' else True

    if not flask_app.nconst_re.match(nconst):
        flask_app.errorMessage['error'] = u"Invalid name ID"
        return json.dumps(flask_app.errorMessage),status.HTTP_400_BAD_REQUEST, {'Content-Type': 'application/json'}

    #
    # Make sure the name exists.
    #
    result = flask_app.core.names.get_name_by_id(nconst)
    if not result:
        abort(404)

    name_dict = flask_app.core.pretty_printer.name(nconst, show_titles=show_titles, show_roles=show_roles, show_details=show_details)
    return json.dumps(name_dict), status.HTTP_200_OK, {'Content-Type': 'application/json'}

@flask_app.route("/search/name", methods=['GET'])
def search_name():
    q = request.args.get('q')
    if not q:
        flask_app.errorMessage['error'] = u"Invalid search criteria"
        return json.dumps(flask_app.errorMessage),status.HTTP_400_BAD_REQUEST, {'Content-Type': 'application/json'}

    q = urllib.unquote(q).decode('utf-8')

    results = flask_app.core.names.get_name_by_name(q)

    #
    # By default, show details and titles in the search results.
    #
    show_roles = True if request.args.get('show_roles') == 'true' else False
    show_details = False if request.args.get('show_details') == 'false' else True
    show_titles = False if request.args.get('show_titles') == 'false' else True

    output_results = []
    for x in results:
        output_results.append(
            flask_app.core.pretty_printer.name(x, show_titles=show_titles, show_roles=show_roles, show_details=show_details)
        )

    return json.dumps(output_results), status.HTTP_200_OK, {'Content-Type': 'application/json'}

@flask_app.route("/search/title", methods=['GET'])
def search_title():
    q = request.args.get('q')
    if q is None or not q:
        flask_app.errorMessage['error'] = u"Invalid search criteria"
        return json.dumps(flask_app.errorMessage),status.HTTP_400_BAD_REQUEST, {'Content-Type': 'application/json'}

    q = urllib.unquote(q).decode('utf-8')

    results = flask_app.core.titles.get_title_by_name(q)

    #
    # Try a partial match if there are no matches and the
    # search string is not too short.
    #
    if not results and len(q) > 4:
        results = flask_app.core.titles.starts_with(q)

    #
    # By default, show the cast information in the search results
    #
    show_roles = True if request.args.get('show_roles') == 'true' else False
    show_cast = False if request.args.get('show_cast') == 'false' else True

    output_results = []
    if results is not None:
        for x in results:
            output_results.append(
                flask_app.core.pretty_printer.title(x, show_roles=show_roles, show_cast=show_cast)
            )

    return json.dumps(output_results), status.HTTP_200_OK, {'Content-Type': 'application/json'}

def main():
    app = App()

    flask_app.core = app
    flask_app.tconst_re = re.compile("^tt[0-9]+$")
    flask_app.nconst_re = re.compile("^nm[0-9]+$")
    flask_app.errorMessage = {
        'error': ''
    }

    try:
        #
        # Mainline.  Run our simple app in single threaded mode.
        #
        flask_app.run(
            host="0.0.0.0",
            port=app.config.listen_port,
            threaded=False,
            processes=1
        )

    except Exception:
        logging.exception("Exception occurred.  Exiting")
        return False

    print("Exiting")
    return True

if __name__ == "__main__":
    sys.exit(0 if main() else 1)
